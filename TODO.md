# NVIM
- [] Keybindings
- [] Whichkey
- [] 

# Hyprland
- [] Theme
- [] Keybind webbrowser
- [] 

# Waybar
- [] Colors
- [] nm applet theme
- [] RAM useage
- [] CPU temp
- [] Icons for desktop page nums
- [] 

# Drun
- [] Theme

# TMUX
- [] theme
- [] vim integration
- [] transfer plugins and keybindings

