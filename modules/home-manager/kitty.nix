{pkgs, config, ...}:
{
  programs.kitty = {
    enable = true;
    shellIntegration.enableZshIntegration = true;
    theme = "Catppuccin-Mocha";
    font = {
	name = "JetBrainsMono Nerd Font Mono";
	size = 10;
    };
    settings = {
      wayland_titlebar_color = "system";
      hide_window_decorations = "yes";
      enable_audio_bell = false;
      background_opacity = "0.85";
      dynamic_background_opacity = "yes";
    };
  };
}
