{pkgs, config, ...}:
{
  programs.nixvim = { enable = true;
    enableMan = true;
    colorschemes.catppuccin.enable = true;
    viAlias = true;
    vimAlias = true;
    clipboard.providers.wl-copy.enable = true;
    options = {
        number = true;
        relativenumber = true;
        shiftwidth = 4;
        tabstop = 4;
        softtabstop = 4;
        expandtab = true;
        smartindent = true;
        wrap = true;
        hlsearch = true;
        incsearch = true;
        scrolloff = 8;
        signcolumn = "yes";
        colorcolumn = "100";
    };

    keymaps = [
        { 
            action = ":m '>+1<CR>gv=gv";
            key = "J";
            mode = ["v"];
        }
        { 
            action = ":m '<+2<CR>gv=gv";
            key = "K";
            mode = ["v"];
        }
        # Window
        { 
            # Split horisontal
            action = ":split<CR>";
            key = "<leader>ws";
            mode = ["n"];
        }
        {
            # Split vertical
            action = ":vsplit<CR>";
            key = "<leader>wv";
            mode = ["n"];
        }
        {
            # Split vertical
            action = ":close<CR>";
            key = "<leader>wd";
            mode = ["n"];
        }
        {
            # Move left
            action = "<C-w>h";
            key = "<leader>wh";
            mode = ["n"];
        }
        {
            # Move right
            action = "<C-w>l";
            key = "<leader>wl";
            mode = ["n"];
        }
        {
            # Move up
            action = "<C-w>k";
            key = "<leader>wk";
            mode = ["n"];
        }
        {
            # Move down
            action = "<C-w>j";
            key = "<leader>wj";
            mode = ["n"];
        }
        # TMUX
        {
            # Move left
            action = ":TmuxNavigateLeft<CR>";
            key = "<C-h>";
            mode = ["n"];
        }
        {
            # Move right
            action = ":TmuxNavigateRight<CR>";
            key = "<C-l>";
            mode = ["n"];
        }
       {
            # Move up
            action = ":TmuxNavigateUp<CR>";
            key = "<C-k>";
            mode = ["n"];
        }
       {
            # Move down
            action = ":TmuxNavigateDown<CR>";
            key = "<C-j>";
            mode = ["n"];
        }
        # LSP
       {
            action = ":Telescope lsp_references<CR>";
            key = "<leader>lr";
            mode = ["n"];
        }
       {
            action = "vim.lsp.buf.rename";
            key = "<leader>lR";
            mode = ["n"];
            lua = true;
        }
       {
            action = ":Telescope lsp_document_symbols<CR>";
            key = "<leader>ls";
            mode = ["n"];
        }
       {
            action = ":Telescope diagnostics<CR>";
            key = "<leader>ld";
            mode = ["n"];
        }
        # File
        {
            action = ":Telescope find_files<CR>";
            key = "<leader>ff";
            mode = ["n"];
        }
        {
            action = ":Telescope live_grep<CR>";
            key = "<leader>fg";
            mode = ["n"];
        }
        {
            action = ":Telescope buffers<CR>";
            key = "<leader>fb";
            mode = ["n"];
        }
    ];

    globals.mapleader = " ";


    plugins.nvim-cmp = {
        enable = true;
        autoEnableSources = true;
            sources = [
              {name = "nvim_lsp";}
              {name = "path";}
              {name = "buffer";}
              {name = "luasnip";}
            ];

        mapping = {
            "<C-Space>" = "cmp.mapping.complete()";
            "<C-d>" = "cmp.mapping.scroll_docs(-4)";
            "<C-e>" = "cmp.mapping.close()";
            "<C-f>" = "cmp.mapping.scroll_docs(4)";
            "<CR>" = "cmp.mapping.confirm({ select = true })";
            "<S-Tab>" = "cmp.mapping(cmp.mapping.select_prev_item(), {'i', 's'})";
            "<Tab>" = "cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'})";
        };

        snippet.expand = "luasnip";
    };

    plugins.lsp = {
      enable = true;
      servers.ccls = { 
        enable = true;
	    autostart = true;
      };

      servers.nixd = {
        enable = true;
        autostart = true;
      };

      keymaps.lspBuf = {
          K = "hover";
          gD = "references";
          gd = "definition";
          gi = "implementation";
          gt = "type_definition";
      };
    };

    plugins.which-key = {
    	enable = true;
	    ignoreMissing = false;
        registrations  = {
            "<leader>w" = "window";
            "<leader>wh" = "left";
            "<leader>wj" = "down";
            "<leader>wk" = "up";
            "<leader>wl" = "right";
            "<leader>ws" = "split";
            "<leader>wv" = "vsplit";
            "<leader>wd" = "close";

            "<leader>l" = "lsp";
            "<leader>lr" = "references";
            "<leader>lR" = "rename";
            "<leader>ls" = "symbols";
            "<leader>ld" = "diagnostics";
            
            "<leader>f" = "find";
            "<leader>ff" = "files";
            "<leader>fg" = "grep";
            "<leader>fb" = "buffers";
        };
    }; 






    plugins.cmp-nvim-lsp.enable = true;
   
    plugins.lsp-format.enable = true;
    plugins.treesitter.enable = true;
    plugins.luasnip.enable = true;
    plugins.cmp_luasnip.enable = true;
    plugins.telescope.enable = true;
    plugins.lightline.enable = true;
    plugins.oil.enable = true;

	
    #plugins.which-key.enable = true;
    #extraConfigLua = builtins.readFile ./nvim-dotfiles/plugins/whichkey.lua;
    #extraConfigLua = builtins.readFile ./nvim-dotfiles/remap.lua;
    #extraConfigLua =  ''
    #-- Set background transparent so terminal can decide opacity
    #vim.api.nvim_create_autocmd({"BufEnter", "BufWinEnter"}, {
  	#pattern = {"*"},
  	#command = "highlight Normal ctermbg=NONE guibg=NONE",
    #})
    #'';
  };
}
