vim.g.mapleader = " "
vim.keymap.set("x", "<leader>p", "\"_dP") -- paste without yanking
vim.keymap.set("n", "<leader>P", "\"*p")

vim.keymap.set("n", "<leader>P", "\"*p")
vim.keymap.set("n", "<leader>P", "\"*p")
vim.keymap.set("n", "<leader>P", "\"*p")
vim.keymap.set("n", "<leader>P", "\"*p")
-- yank to system clipboard
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- create new tab
vim.keymap.set("n", "<leader>tc", ":tabnew<CR>")
vim.keymap.set("n", "<leader>tn", ":tabnext<CR>")
vim.keymap.set("n", "<leader>tp", ":tabprevious<CR>")
vim.keymap.set("n", "<leader>td", ":tabclose<CR>")

-- create window
vim.keymap.set("n", "<leader>ws", ":split<CR>")
vim.keymap.set("n", "<leader>wv", ":vsplit<CR>")
vim.keymap.set("n", "<leader>wd", ":close<CR>")
vim.keymap.set("n", "<leader>wh", "<C-w>h")
vim.keymap.set("n", "<leader>wj", "<C-w>j")
vim.keymap.set("n", "<leader>wk", "<C-w>k")
vim.keymap.set("n", "<leader>wl", "<C-w>l")

--
vim.keymap.set("n", "<C-h>", ":TmuxNavigateLeft<CR>")
vim.keymap.set("n", "<C-j>", ":TmuxNavigateDown<CR>")
vim.keymap.set("n", "<C-k>", ":TmuxNavigateUp<CR>")
vim.keymap.set("n", "<C-l>", ":TmuxNavigateRight<CR>")
