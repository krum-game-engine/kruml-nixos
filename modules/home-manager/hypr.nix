{pkgs, config, ...}:
{

wayland.windowManager.hyprland = {
    # Whether to enable Hyprland wayland compositor
    enable = true;
    # The hyprland package to use
    package = pkgs.hyprland;
    # Whether to enable XWayland
    xwayland.enable = true;

    # Optional
    # Whether to enable hyprland-session.target on hyprland startup
    systemd.enable = true;
  };

  wayland.windowManager.hyprland.settings = {
    monitor = "eDP-1,1920x1080@60,0x0,1.25";
    env = "XCURSOR_SIZE,24";
 
    general = {
      gaps_in = 5;
      gaps_out = 7;
      border_size = 1;
      "col.active_border" = "rgba(f5c2e7ee)";
      "col.inactive_border" = "rgba(6c708caa)";
      layout = "dwindle";
      allow_tearing=false;
    };

    decoration = {
      rounding = 5;      
      blur.enabled = true;
      blur.size = 3;
      blur.passes = 1;
    };

    animations = {
      enabled = "yes";
      # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more
      bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
      animation = [
        "windows, 1, 7, myBezier"
        "windowsOut, 1, 7, default, popin 80%"
	"border, 1, 10, default"
	"borderangle, 1, 8, default"
	"fade, 1, 7, default"
	"workspaces, 1, 6, default"
       ];
    };

   "$mainMod" = "SUPER";
   bind = [
	"$mainMod, Q, exec, kitty"
	"$mainMod, C, killactive, "
	"$mainMod, M, exit, "
	"$mainMod, E, exec, dolphin"
	"$mainMod, V, togglefloating, "
	"$mainMod, R, exec, rofi -show drun -show-icons"
	"$mainMod, P, pseudo,"
	"$mainMod, J, togglesplit,"

	# Move focus with mainMod + arrow keys
	"$mainMod, h, movefocus, l"
	"$mainMod, l, movefocus, r"
	"$mainMod, k, movefocus, u"
	"$mainMod, j, movefocus, d"

	# Switch workspaces with mainMod + [0-9]
	"$mainMod, 1, workspace, 1"
	"$mainMod, 2, workspace, 2"
	"$mainMod, 3, workspace, 3"
	"$mainMod, 4, workspace, 4"
	"$mainMod, 5, workspace, 5"
	"$mainMod, 6, workspace, 6"
	"$mainMod, 7, workspace, 7"
	"$mainMod, 8, workspace, 8"
	"$mainMod, 9, workspace, 9"
	"$mainMod, 0, workspace, 10"

	# Move active window to a workspace with mainMod + SHIFT + [0-9]
	"$mainMod SHIFT, 1, movetoworkspace, 1"
	"$mainMod SHIFT, 2, movetoworkspace, 2"
	"$mainMod SHIFT, 3, movetoworkspace, 3"
	"$mainMod SHIFT, 4, movetoworkspace, 4"
	"$mainMod SHIFT, 5, movetoworkspace, 5"
	"$mainMod SHIFT, 6, movetoworkspace, 6"
	"$mainMod SHIFT, 7, movetoworkspace, 7"
	"$mainMod SHIFT, 8, movetoworkspace, 8"
	"$mainMod SHIFT, 9, movetoworkspace, 9"
	"$mainMod SHIFT, 0, exec, wpctl set-volume 61 5%+"

	# Example special workspace (scratchpad)
	"$mainMod, S, togglespecialworkspace, magic"
	"$mainMod SHIFT, S, movetoworkspace, special:magic"

	# Scroll through existing workspaces with mainMod + scroll
	"$mainMod, mouse_down, workspace, e+1"
	"$mainMod, mouse_up, workspace, e-1"

    "$mainMod, u, exec, wpctl set-volume 61 5%+"
    ",XF86MonBrightnessDown,exec,brightnessctl set 5%-"
    ",XF86MonBrightnessUp,exec,brightnessctl set 5%+"

	# Move/resize windows with mainMod + LMB/RMB and dragging
   ]; 

   bindm = [
	"$mainMod, mouse:272, movewindow"
	"$mainMod, mouse:273, resizewindow"
   ];

   binde = [
    ", XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
    ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
   ];

   bindl = [
    ", XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
    ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
   ];

   exec-once = "bash ~/.config/hypr/start.sh";
   misc.force_default_wallpaper = 0;

  };

}

